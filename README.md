Dekoratyvinis apželdinimas - studijų programa skirta rengti želdininkystės specialistus, kurie, vadovaudamiesi darnaus vystimosi principais, gali spręsti želdynų kokybės gerinimo ir kraštovaizdžio tausojimo problemas, kompetentingai ir kūrybiškai organizuoti želdininkystės verslą, integruoti ir taikyti profesinėje veikloje naujausias mokslo žinias, Lietuvoje bei kitose valstybėse sukauptą patirtį. ​


